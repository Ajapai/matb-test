class Symbolsystem {
    #axiom;
    #factor;
    #axiomAngle;
    #patternAngle;
    #animationAngle;
    
    #lines = []; // Stores dots of drawn lines as if spawned on position 0, 0
    #copyPositions = [];

    #positionStack = [];
    #startPos = {x: null, y: null, angle: null, length: null};
    #currentPos = {x: null, y: null, angle: null, length: null};
    #multiplier = 1;

    draw() {
      this.#drawBase();
      this.#drawCopies();
    }

    #drawBase() {
      const axiomArray = this.#axiom.split("");
      axiomArray.forEach((symbol) => {
          this.#executeSymbol(symbol);
      })
    }

    #drawCopies() {
      this.#copyPositions.forEach((pos) => {
        this.#lines.forEach((li) => {
          line(li.x1 + pos.x, li.y1 + pos.y, li.x2 + pos.x, li.y2 + pos.y)
        })
      })
    }

    drawNextAnimationFrame() {
        this.#lines = [];
        this.#patternAngle += this.#animationAngle;
        this.#animationAngle *= -1;

        this.#currentPos = {...this.#startPos};
        this.draw();
    }

    constructor(rules) {
        this.#currentPos.x = 0;
        this.#currentPos.y = 0;
        this.#currentPos.angle = rules.startAngle;

        this.#generateAxiom(rules.axiom, rules.pattern, rules.times);
        this.#factor = rules.factor;
        this.#patternAngle = rules.patternAngle;
        this.#axiomAngle = rules.axiomAngle;
        this.#animationAngle = rules.animationAngle;

        this.#currentPos.length = Math.round(this.#factor * 100);

        this.#startPos = {...this.#currentPos};
    }

    addCopy(position) {
      this.#copyPositions.push(position);
    }

    #executeSymbol(symbol) {
        switch(symbol) {
          case "F":
            const x1 = this.#currentPos.x;
            const y1 = this.#currentPos.y;
            this.#addFixedLenght();
            this.#lines.push({x1: x1, y1: y1, x2: this.#currentPos.x, y2: this.#currentPos.y})
            break;
          case "G":
            this.#addFixedLenght();
            break;
          case "+":
            this.#addAngle(true, this.#patternAngle);
            break;
          case "-":
            this.#addAngle(false, this.#patternAngle);
            break;
          case ">":
            this.#addAngle(true, this.#axiomAngle);
            break;
          case "<":
            this.#addAngle(false, this.#axiomAngle);
            break;
          case "[":
            this.#positionStack.push({...this.#currentPos});
            break;
          case "]":
            this.#currentPos = {...this.#positionStack.pop()};
            break;
          case "|":
            this.#currentPos.length *= this.#factor;
            const x = this.#currentPos.x;
            const y = this.#currentPos.y;
            this.#addFixedLenght();
            this.#lines.push({x1: x, y1: y, x2: this.#currentPos.x, y2: this.#currentPos.y})
            break;
          case "_":
            this.#currentPos.length = Math.round(this.#factor * 100);
            break;
          default:
            let num = Number(symbol);
            if (isNaN(num)) break;
            this.#multiplier = num;
            break;
        }
    }

    #addFixedLenght() {
        this.#currentPos.x = this.#currentPos.length * cos(this.#currentPos.angle) + this.#currentPos.x,
        this.#currentPos.y = this.#currentPos.length * sin(this.#currentPos.angle) + this.#currentPos.y
    }

    #addAngle(toRight, angle) {
        for (let i = 0; i < this.#multiplier; i++) {
          this.#currentPos.angle = (this.#currentPos.angle + (toRight ? angle : -angle)) % 360;
        }
      
        this.#multiplier = 1;
    }

    #generateAxiom(axiom, pattern, times) {
        this.#axiom = axiom.replaceAll("+", ">").replaceAll("-", "<");
        for (let i = 0; i < times; i++) {
          this.#axiom = this.#axiom.replaceAll("F", pattern);
        }
    }
}