// Elias Gemperle
// Inspiration
/*
  Wälder so alt wie Hügel die Sonnige Flecken des Grüns umschliessen.

  1. Ziel einen Baum mit symbolsystem generieren.

  2. Den Baum kopieren um ihn an mehreren Orten zu plazieren.

  3. Zufällig Bäume generieren lassen.

  4. Animation ist leider noch nicht fertig.
*/

const treeRules = {
  pattern: "|[F[3-F][3+F]|[3-F][3+F]]",
  axiom: "F",
  times: 3,
  factor: 0.65,
  patternAngle: 45,
  axiomAngle: 120,
  animationAngle: 1,
  startAngle: 270
}

let symbolsystem;

function setup() {
  createCanvas(1200, 600);
  angleMode(DEGREES);
  stroke(17, 101, 48)

  symbolsystem = new Symbolsystem(treeRules);

  generateRandomTrees();
  generateRandomTrees();

  background(24, 165, 88);
  symbolsystem.draw()

}

/*
function draw() {
    background(24, 165, 88);
    symbolsystem.drawNextAnimationFrame();
}
*/

function generateRandomTrees() {
  let approximateTreeSizeX = 35;
  let approximateTreeSizeY = 120;

  let currentX = -50;
  let currentY = -50;

  while(currentX < width + 50) {
    currentX += random(approximateTreeSizeX / 2, approximateTreeSizeX * 2);
    currentY += random(approximateTreeSizeY / 2, approximateTreeSizeY * 2);

    if (currentY > height + 50) currentY -= height + 100;

    symbolsystem.addCopy({x: currentX, y: currentY})
  }
}
